INSERT INTO `rentbicycle_db`.`users` (`login`, `pass`) VALUES ('guest', 'guest');


INSERT INTO `rentbicycle_db`.`articles`
(
`title`,
`text`,
`date`)
VALUES
(
'CRUISER Dallas ALUMINIUM 2023',
'
Рама/размер: алюминий/19 дюймов;<br>
Шатун: однорядный;<br>
Опора: на картриджных подшипниках;<br>
Передняя ступица: сталь;<br>
Задний переключатель: Shimano Nexus 3 скорости (ступица);<br>
Обода: усиленный конический алюминий;<br>
Шины: 28x1,75;<br>
Рулевое колесо: хромированная сталь;<br>
Вынос руля: сталь;<br>
Седло: на пружине;<br>
Педали: пластик;<br>
Крылья, багажник, подножка, защита цепи, светодиодное освещение;<br>
Отражатели на колесах;<br>
Звонок.
',
 CURRENT_TIMESTAMP
);

INSERT INTO `rentbicycle_db`.`articles`
(
`title`,
`text`,
`date`)
VALUES
(
'POLYGON Trid ZZ 26 2021',
'
Рама: AL6 SLOPE STYLE, 100MM TRAVEL, POSTMOUNT 160MM;<br>
Размер: колеса 26.0;<br>
Вилка: ROCKSHOX PIKE DJ, TRAVEL 100MM, TAPERED STEERER;<br>
Задний амортизатор: ROCKSHOX MONARCH R, 7.5”X2.00”(190X51MM), M8X22MM;<br>
Манетка правая/задняя: SRAM SL-GX-DH-A2 7-SPEED;<br>
Задний переключатель: SRAM RD-GX-1DH-A3 DA;<br>
Рулевая колонка: ZS 44/28.6 | ZS 56/40;<br>
Вынос: ALLOY E:40MM R:0DEG BB:31.8MM;<br>
Руль: ALLOY W:750MM R:38.1MM BB:3.18MM;<br>
Передний тормоз: SHIMANO BR- MT501;<br>
Задний тормоз: SHIMANO BR- MT501;<br>
Система/Шатуны: SAMOX TAF15-D32A 32T, 170MM;<br>
Каретка: SAMOX INTEGRATED BB BSA;<br>
Кассета: SRAM PG-720 7-SPEED 11-25T;<br>
Цепь: KMC X11;<br>
Обода: ALLOY DOUBLE WALL W/ ALLOY HUB 6-BOLT;<br>
Покрышки: VEE XCV, 26”X2.25”;<br>
Седло: POLYGON PIVOTAL SADDLE;<br>
Подседельный штырь: ALLOY, PIVOTAL, 30.9X200MM;<br>
Кол-во: скоростей 7.
',
 CURRENT_TIMESTAMP
);


INSERT INTO `rentbicycle_db`.`articles`
(
`title`,
`text`,
`date`)
VALUES
(
'SLR Sram Red 2021',
'
Рама: Carbon Monocoque HUS MOD + Crystal Liquid Polymer;<br>
Размер колеса: 28.0;<br>
Вилка: ZERO SLR - Carbon Monocoque HUS MOD + Crystal Liquid Polymer;<br>
Манетка правая/задняя: Sram Red ETap AXS Disc;<br>
Манетка левая/передняя: Sram Red ETap AXS Disc;<br>
Передний переключатель: Sram Red ETap AXS FD-RED-E-D1;<br>
Задний переключатель: Sram Red ETap AXS RD-RED-E-D1;<br>
Вынос: Wilier Stemma 90/100/110/120;<br>
Руль: Zero Integraded Carbon Bar;<br>
Грипсы: Prologo One Touch;<br>
Передний тормоз: Sram Red ETap AXS HRD Centerlock 160mm;<br>
Задний тормоз: Sram Red ETap AXS HRD Centerlock 160mm;<br>
Система/Шатуны: Sram Red Fc-Red-D1 46-33t;<br>
Каретка: Sram Dub Pressfit Bb-Dub-Pf-A1;<br>
Кассета: Sram Red Xg-1290 10-28t 12 Speed;<br>
Цепь: Sram Red Flat Top;<br>
Обода: Wilier Triestina Ult38kt Carbon - Tubolar;<br>
Покрышки: Vittoria Corsa 700x25 Tubolar;<br>
Передняя втулка: Wilier Triestina Ult38kt Carbon - Tubular, Thru Axle Mavic Speed Release 12X100;<br>
Задняя втулка: Wilier Triestina Ult38kt Carbon - Tubular, Thru Axle Mavic Speed Release 12X142;<br>
Седло: Selle Italia;<br>
Подседельный штырь: Zero SLR Carbon Wilier Custom Made;<br>
Кол-во скоростей: 24;<br>
Вес: 6.6 кг.
',
 CURRENT_TIMESTAMP
);

INSERT INTO `rentbicycle_db`.`groupuser` (`name`, `users_login`) VALUES ('default', 'guest');


INSERT INTO `rentbicycle_db`.`groupuser_has_articles` (`groupuser_name`, `articles_id`) VALUES ('default', 1);

INSERT INTO `rentbicycle_db`.`groupuser_has_articles` (`groupuser_name`, `articles_id`) VALUES ('default', 2);

INSERT INTO `rentbicycle_db`.`groupuser_has_articles` (`groupuser_name`, `articles_id`) VALUES ('default', 3);
