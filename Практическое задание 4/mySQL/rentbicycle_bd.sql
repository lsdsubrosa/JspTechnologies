-- MySQL Script generated by MySQL Workbench
-- Tue Mar 14 20:33:14 2023
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema rentbicycle_db
-- -----------------------------------------------------
-- База Данных для сайта RentBicycle

-- -----------------------------------------------------
-- Schema rentbicycle_db
--
-- База Данных для сайта RentBicycle
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rentbicycle_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `rentbicycle_db` ;

-- -----------------------------------------------------
-- Table `rentbicycle_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rentbicycle_db`.`users` (
  `login` VARCHAR(15) NOT NULL COMMENT 'Логин',
  `pass` VARCHAR(45) NULL COMMENT 'Пароль',
  PRIMARY KEY (`login`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `rentbicycle_db`.`groupuser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rentbicycle_db`.`groupuser` (
  `name` VARCHAR(20) NOT NULL COMMENT 'Наименование группы',
  `users_login` VARCHAR(15) NOT NULL COMMENT 'Вторичный ключ от таблицы users',
  PRIMARY KEY (`name`),
  INDEX `fk_groupuser_users` (`users_login` ASC) ,
  CONSTRAINT `fk_groupuser_users`
    FOREIGN KEY (`users_login`)
    REFERENCES `rentbicycle_db`.`users` (`login`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `rentbicycle_db`.`articles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rentbicycle_db`.`articles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL COMMENT 'Заголовок статьи',
  `text` TEXT NOT NULL COMMENT 'Текст статьи',
  `date` TIMESTAMP NOT NULL DEFAULT now() COMMENT 'Дата добавления статьи',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;



-- -----------------------------------------------------
-- Table `rentbicycle_db`.`groupuser_has_articles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rentbicycle_db`.`groupuser_has_articles` (
  `groupuser_name` VARCHAR(20) NOT NULL,
  `articles_id` INT NOT NULL,
  PRIMARY KEY (`groupuser_name`, `articles_id`),
  INDEX `fk_groupuser_has_articles_articles1` (`articles_id` ASC),
  INDEX `fk_groupuser_has_articles_groupuser1` (`groupuser_name` ASC),
  CONSTRAINT `fk_groupuser_has_articles_articles1`
    FOREIGN KEY (`articles_id`)
    REFERENCES `rentbicycle_db`.`articles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groupuser_has_articles_groupuser1`
    FOREIGN KEY (`groupuser_name`)
    REFERENCES `rentbicycle_db`.`groupuser` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `rentbicycle_db`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rentbicycle_db`.`messages` (
  `id` INT NOT NULL,
  `text` VARCHAR(255) NOT NULL COMMENT 'Текст сообщения',
  `date` TIMESTAMP NOT NULL DEFAULT now() COMMENT 'Дата сообщения',
  `users_login` VARCHAR(15) NOT NULL COMMENT 'Пользователь пославший сообщение',
  `articles_id` INT NOT NULL COMMENT 'Статья, к которой оставлен комментарий',
  PRIMARY KEY (`id`),
  INDEX `fk_messages_users1` (`users_login` ASC),
  INDEX `fk_messages_articles1` (`articles_id` ASC),
  CONSTRAINT `fk_messages_articles1`
    FOREIGN KEY (`articles_id`)
    REFERENCES `rentbicycle_db`.`articles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`users_login`)
    REFERENCES `rentbicycle_db`.`users` (`login`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
