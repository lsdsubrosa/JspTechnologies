<%-- 
    Document   : article
    Created on : 12 мар. 2023 г., 15:19:21
    Author     : http://onedeveloper.ru/, Andrey Petrov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    </div>
    <div class="around-border">
        <div class="bicycle-question">
            <div class="bicycle-type"><a href="http://localhost:8080/RentBicycle/choose.jsp">Велосипеды в наличии</a>
        </div>
            <div class="ask-question"><a href="#">Задать вопрос</a></div>
        </div>
        <div class="terms_region">
            <div class="terms_name">
                <h1>Условия и цены</h1>
            </div>
            <div class="prices_table">
                <table class="table_terms">
                    <tr>
                        <td>Количество часов</td>
                        <th>Стоимость</th>
                    </tr>
                    <tr>
                        <td>1 час</td>
                        <th>200 у.е.</th>
                    </tr>
                    <tr>
                        <td>5 часов</td>
                        <th>700 у.е.</th>
                    </tr>
                    <tr>
                        <td>12 часов</td>
                        <th>1000 у.е.</th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="btn-region">
            <button class="btn call-button" type="submit">Заказать обратный звонок</button>
        </div>
    </div>

    <div class="articles around-border">
    <section>
        <c:forEach var="article" items="${articles}">
            <div class="article-name margin-bottom-ten">
                <h1>${article.title}</h1>
            </div>

            <div class="article-text margin-bottom-ten">
                <div class="text-article">
                    ${fn:substring(article.text,0,128)} ...
                </div>
            </div>
            
            <div class="article-information">
                <span class="read"><a href="article?id=${article.id}">Читать далее</a></span><br>
                <span class="autor-name">Автор статьи: <a href="#">RentBicycleSite</a></span><br>
                <span class="article-date">Дата статьи: ${article.date}</span>
            </div>
        </c:forEach>
    </section>
