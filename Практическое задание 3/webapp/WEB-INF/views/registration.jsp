<%-- 
    Document   : Registration
    Created on : 12 мар. 2023 г., 16:41:04
    Author     : http://onedeveloper.ru/, Andrey Petrov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    </div>
    <div class="around-border">
        <div class="main-block-name">
            <h1>Регистрация</h1>
        </div>
        <div class="labels-region flex-column-center">
            <form action="#">
                <label for="login">Логин:</label><br>
                <input required type="text" id="login" name="login" value="Ваш логин"><br><br>

                <label for="email">Почта:</label><br>
                <input required type="text" id="email" name="email" value="почта@пчт.ру"><br><br>

                <label for="pass">Пароль:</label><br>
                <input required type="text" id="pass" name="pass" value="пароль123"><br><br>

                <div class="btn-region">
                    <button class="btn reg-btn" type="submit" formmethod="post">Зарегистрироваться</button>
                </div>
            </form>
        </div>
    </div>
