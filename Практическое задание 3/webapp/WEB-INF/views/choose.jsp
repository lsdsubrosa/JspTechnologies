<%-- 
    Document   : index
    Created on : 12 мар. 2023 г., 16:42:11
    Author     : http://onedeveloper.ru/, Andrey Petrov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    </div>
    <div class="around-border">
        <div class="bicycle-choose flex-column-center">
            <h1 class="choose-text">Тип велосипеда:</h1>

            <div class="bike-type flex-column-center">

                <div class="city-bike flex-column-center margin-bottom-ten">
                    <p class="bike-type-text">Городской:</p>
                    <img src="img/city-bike.jpeg"
                         class="bike-image"
                         alt="city bicycle"
                         style="width: 250px;">
                    <p class="bike-name-text">CRUISER Dallas ALUMINIUM 2023</p>
                </div>

                <div class="mtb-bike flex-column-center margin-bottom-ten">
                    <p class="bike-type-text">Горный:</p>
                    <img src="img/mtb-bike.jpg"
                         class="bike-image"
                         alt="mtb bicycle"
                         style="width: 250px;">
                    <p class="bike-name-text">POLYGON Trid ZZ 26 2021</p>
                </div>

                <div class="highway-bike flex-column-center">
                    <p class="bike-type-text">Шоссейный:</p>
                    <img src="img/highway-bike.png"
                         class="bike-image"
                         alt="highway bicycle"
                         style="width: 250px;">
                    <p class="bike-name-text">SLR Sram Red 2021</p>
                </div>
            </div>
        </div>
    </div>
