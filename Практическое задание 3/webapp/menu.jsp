<%-- 
    Document   : article
    Created on : 12 мар. 2023 г., 15:19:21
    Author     : http://onedeveloper.ru/, Andrey Petrov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    </div>
    <div class="around-border">
        <div class="bicycle-question">
            <div class="bicycle-type"><a href="#">Велосипеды в наличии</a></div>
            <div class="ask-question"><a href="#">Задать вопрос</a></div>
        </div>
        <div class="terms_region">
            <div class="terms_name">
                <h1>Условия и цены</h1>
            </div>
            <div class="prices_table">
                <table class="table_terms">
                    <tr>
                        <td>Количество часов</td>
                        <th>Стоимость</th>
                    </tr>
                    <tr>
                        <td>1 час</td>
                        <th>200 у.е.</th>
                    </tr>
                    <tr>
                        <td>5 часов</td>
                        <th>700 у.е.</th>
                    </tr>
                    <tr>
                        <td>12 часов</td>
                        <th>1000 у.е.</th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="btn-region">
            <button class="btn call-button" type="submit">Заказать обратный звонок</button>
        </div>
    </div>
