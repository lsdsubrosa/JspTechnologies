<%-- 
    Document   : Registration
    Created on : 12 мар. 2023 г., 16:41:04
    Author     : http://onedeveloper.ru/, Andrei Petrov
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Выбор велосипедов</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="_container-maxwidth">
    <div class="header_region">
        <div class="header_name">
            <h1>Регистрация</h1>
        </div>
    </div>
    <div class="around-border">
        <div class="labels-region flex-column-center">
            <form action="#">
                <label for="login">Логин:</label><br>
                <input required type="text" id="login" name="login" value="Ваш логин"><br><br>

                <label for="email">Почта:</label><br>
                <input required type="text" id="email" name="email" value="почта@пчт.ру"><br><br>

                <label for="pass">Пароль:</label><br>
                <input required type="text" id="pass" name="pass" value="пароль123"><br><br>

                <div class="btn-region">
                    <button class="btn reg-btn" type="submit">Зарегистрироваться</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
